package com.example.demo;

import com.example.demo.controller.FilmsController;
import com.example.demo.service.FilmsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FilmTest {
    private MockMvc mockMvc;

    @Mock
    FilmsService filmsService;
    @Mock
    private FilterChainProxy springSecurityFilterChain;

    @InjectMocks
    FilmsController filmsController;

    @Autowired
    WebApplicationContext context;

    @BeforeEach
    void set_up(){
        MockMvc mockMvc = MockMvcBuilders.
                webAppContextSetup(context)
                .alwaysDo(print())
                .apply(SecurityMockMvcConfigurers.springSecurity(springSecurityFilterChain))
                .build();
    }

    @Test //delete test
    public void CustomerDeleteTest(){
        ResponseEntity<?> responseEntity = filmsController.deleteFilm(1);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        System.out.println("test Succesful");

    }
}
